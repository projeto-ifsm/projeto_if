<?php

session_start();
include_once("conecta.php");

$msg = array();

try {


  if ($_GET)
    {

$cpf = filter_var($_GET['cpf'], FILTER_SANITIZE_STRING) ?: throw new Exception('Preencha o CPF!');
$senha = filter_var($_GET['senha'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES) ?: throw new Exception('Por favor, preencha a senha!');


$cpf = mysqli_real_escape_string($conn, $cpf);
$senha = mysqli_real_escape_string($conn, $senha);

$query = "select * from cliente where cpf = '{$cpf}' and senha = '{$senha}'";

$result = mysqli_query($conn, $query);

$row = mysqli_num_rows($result);

if($row == 1) {
  $_SESSION['cpf'] = $cpf;
	header('Location: areaCliente.php');
	exit();
} else {
	$_SESSION['nao_autenticado'] = true;
	header('Location: inscricao.php');
	exit();
}


    }
}
catch(Exception $ex)
{
    $msg = array(
        'classe' => 'alert-danger',
        'mensagem' => $ex->getMessage()
    );
}



?>


<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="Restaurante IF PHP" />
  <meta name="description" content="Projeto Interdisciplinar" />
  <meta name="author" content="Gisele - Manu - Welton" />
  <link rel="shortcut icon" href="img/favicon.png" type="">
<!-- Bootstrap + Custom CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css" />
<!-- font awesome style -->
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <title> Restaurante IF </title>
</head>

  <body>


 <?php if ($msg) : ?>
            <div class="alert <?= $msg['classe'] ?>">
                <?= $msg['mensagem']; ?>
            </div>
 <?php endif; ?>

      
  <!-- Modal -->
<div class="modal" id="loginModal" tabindex="-1" aria-labelledby="modallogin" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModal">Logar-se</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">     
          </button>
      </div>
      <div class="modal-body">
        <form method="GET">
          <div class="form-group">
            <label for="loginCPF">CPF</label>
            <input type="text" class="form-control" id="loginCPF"  name="cpf" value="<?= $_GET['cpf'] ?? '' ?>">
          </div>
          <div class="form-group">
            <label for="loginSenha">Senha</label>
            <input type="password" class="form-control" id="loginSenha" name="senha" value="<?= $_GET['senha'] ?? '' ?>">
          </div>
          <button type="submit" class="btn btn-success">Entrar na Conta</button>
          <small class="form-text text-muted">Esqueceu a senha? <a href="index.php">Clique aqui</a>.</small>
        </form>
      </div>
    </div>
  </div>
</div>
  <!-- Fecha Modal  -->

<!-- NAVBAR MENU -->
    <nav class="navbar navbar-expand-md navbar-dark nav-bar-color fixed-top py-3 box-shadow">
      <a href="index.php" class="navbar-brand">
        <img src="img/Restauranteif.svg" class="mx-5" alt="Restauranteif">
      </a>
  
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
  
      <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto  mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="contato.php">Contato</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#" data-bs-toggle="modal" data-bs-target="#loginModal">Login</a>
          </li>
          <li class="nav-item">
            <a class="btn btn-outline-success ml-md-5 mx-2" href="inscricao.php">Cadastre-se</a>
          </li>
        </ul>
      </div>
    </nav>
    <!-- Caroussel-->
    <section class="container-fluid">
      <div class="row info-bg text-white">

    <div id="carouselExampleIndicators" class="carousel slide col-lg-7 p-0" data-bs-ride="carousel">
      <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
      </div>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="img/local/foto-4.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="img/local/foto-2.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="img/local/foto-3.jpg" class="d-block w-100" alt="...">
        </div>
      </div>
      <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>
      </div>
    <!-- Infos-->
      <div class="col-lg-5 p-4 align-self-center">
      <h1 class="display-4">Você nunca viu tanta variedade! </h1>
      <h4 class="display-6">Cadastre-se já para fazer sua reserva! </h4>
      <p class="lead">Veja nosso cardápio e conheça nossa gastronomia.</p>
            <a href="cardapio.php" class="btn btn-success btn-lg enable" tabindex="1" role="button" aria-disabled="false">Cardápio</a>
      </div>
    </div>
    </section>
    <!-- Cardápio -->
    <section>
    <section class="py-5 text-center text-white">
      <div class="container">
        <div class="my-6">
          <span class="h2 d-block">Nossas especialidades</span>
        </div>
        <div class="row">
          <div class="col-xl-4 col-md-6">
            <div style="height: 100px;" class="d-flex justify-content-center">
              <img src="img/local/001.jpg" alt="Comida Japonesa">
            </div>
            <h3>Japonesa</h3>
            <p>Aprecie os principais pratos da comida japonesa: suchis, sachimis, temakis, entre outros.</p>
          </div>
  
          <div class="col-xl-4 col-md-6 p-1">
            <div style="height: 100px;" class="d-flex justify-content-center">
              <img src="img/local/002.jpg" alt="Comida Mineira">
            </div>
            <h3>Mineira</h3>
            <p>Se sinta abraçado pela deliciosa comida mineira: feijão tropeiro, frango caipira e nosso famoso doce de leite.</p>
          </div>
  
          <div class="col-xl-4 col-md-6">
            <div style="height: 100px;" class="d-flex justify-content-center">
              <img src="img/local/003.jpg" alt="Lanches">
            </div>
            <h3>Lanches</h3>
            <p>Tenha de forma prática nossos principais lanches: hamburgueres, cahorros-quentes, sanduíches e mais.</p>
          </div>
  
          <div class="col-xl-4 col-md-6">
            <div style="height: 100px;" class="d-flex justify-content-center">
              <img src="img/local/004.jpg" alt="Drinks e Vinhos">
            </div>
            <h3>Drinks & Vinhos</h3>
            <p>Aprecie os melhores vinhos e drinks oferecidos e preparados em nossa casa.</p>
          </div>
  
          <div class="col-xl-4 col-md-6">
            <div style="height: 100px;" class="d-flex justify-content-center">
              <img src="img/local/005.jpg" alt="Chopps">
            </div>
            <h3>Chopp</h3>
            <p>Aproveite nosso chopp geladinho enquanto desfruta do nosso cardápio.</p>
          </div>
  
          <div class="col-xl-4 col-md-6">
            <div style="height: 100px;" class="d-flex justify-content-center">
              <img src="img/local/006.jpg" alt="Churrasco">
            </div>
            <h3>Churrasco</h3>
            <p>Aproveite os melhores cortes de carnes com o nosso churrasco: picanhas, costelas, entre outros.</p>
          </div>
        </div>
      </section> -->
    <!-- Page Quote-->
     <section id="home-quote" class="p-md-5">
    <blockquote class="blockquote text-center text-white p-md-0 p-0">
      <p class="display-4 texto"><em>"A felicidade não é algo pronto. Ela é feita das suas próprias ações."</em></p>
      <footer id="autor">Dalai Lama</footer>
    </blockquote>
  </section>
  <!-- footer section -->
  <footer class="footer_section">
    <div>
        <div class="container">
            <div class="row">
              <div class="col-md-3 col-6">
                <h4>PÁGINAS</h4>
                <ul class="list-unstyled">
                  <li><a href="contato.php" class="custom-nav">Contato</a></li>
                  <li><a href="inscricao.php" class="custom-nav" >Resgistre-se</a></li>
                  <li><a href="login.php" class="custom-nav" data-bs-toggle="modal" data-bs-target="#loginModal">Login</a></li>
                </ul>
              </div>
              <div class="col-md-3 col-6">
                <h4>FILIAIS</h4>
                <ul class="list-unstyled">
                  <li>Poços de Caldas MG</li>
                  <li>Av. Paulista SP</li>
                  <li>Barra da Tijuca RJ</li>
                </ul>
              </div>
              <div class="footer_contact col-md-4">
                <h4>
                  CONTATO
                </h4>
                <div class="contact_link_box">
                  <a href="#">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <span>
                      Central POÇOS DE CALDAS
                    </span>
                  </a>
                  <a href="#">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <span>
                      Tel +35 99199-7070
                    </span>
                  </a>
                  <a href="#">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <span>
                      grupo@alunos.ifsuldeminas.edu.br
                    </span>
                  </a>
                </div>
              </div>
              <div class="col-md-2">
                <h4>SOCIAL MEDIA</h4>
                <div class="footer_social">
                    <a href="https://pt-br.facebook.com/">
                      <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                    <a href="https://twitter.com/login?lang=pt">
                      <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                    <a href="https://br.linkedin.com/">
                      <i class="fa fa-linkedin" aria-hidden="true"></i>
                    </a>
                    <a href="https://www.instagram.com/">
                      <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                  </div>
              </div>
            </div>
          </div>
      <div class="footer-info">
        <p>
          &copy; <span id="displayYear"></span> Todos os direitos reservados By
          <a href="#">Gisele / Manoela / Welton</a><br><br>
        </p>
      </div>
    </div>
  </footer>
  <!-- FIM footer section -->
  <!-- JavaScript Bundle with Popper -->
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
      crossorigin="anonymous"
    ></script>
  </body>
</html>