<?php

session_start();
include_once("conecta.php");


$msg = array();

try 
{

  $cpf = $_SESSION['cpf'];

  $sql_busca = "SELECT * FROM cliente where cpf = $cpf";

    if ($_GET && isset($_GET['excluir'])){

        $cpf = filter_var($_GET['excluir'], FILTER_SANITIZE_STRING);

        

        if($cpf === false){
            throw new Exception("Cliente inválido para exclusão");
        }

        $sql = "DELETE FROM cliente WHERE cpf = $cpf";
        $resultado = mysqli_query($conn, $sql);

        if ($resultado === false || mysqli_errno($conn)) {
            throw new Exception('Erro ao realizar a exclusão no banco de dados: ' . mysqli_error($conn));
        }

         $msg = array(
            'classe' => 'alert-success',
            'mensagem' => 'Excluído com sucesso!'
        );
    }
    
}
catch(Exception $ex)
{
    $msg = array(
        'classe' => 'alert-danger',
        'mensagem' => $ex->getMessage()
    );
}
finally {

    $resultado = mysqli_query($conn, $sql_busca);

    if ($resultado) {
    
        $lista_cliente = mysqli_fetch_all($resultado, MYSQLI_ASSOC);
    }
}


?>


<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="Restaurante IF PHP" />
  <meta name="description" content="Projeto Interdisciplinar" />
  <meta name="author" content="Gisele - Manu - Welton" />
  <link rel="shortcut icon" href="img/favicon.png" type="">
<!-- Bootstrap + Custom CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css" />
<!-- font awesome style -->
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <title> Restaurante IF | Área restrita</title>
</head>
  <body> 

 
    <!-- NAVBAR MENU + Logo -->
    <nav class="navbar navbar-expand-md navbar-dark nav-bar-color fixed-top py-3 box-shadow">
      <a href="index.php" class="navbar-brand">
        <img src="img/Restauranteif.svg" class="mx-5" alt="Restauranteif">
      </a>
    <!-- Button Responsivo -->
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>


    <!-- Navbar Area do cliente -->
      <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
        <ul class="navbar-nav ml-auto  mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="reserva.php">Reservas</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contato.php">Contato</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logout.php">Fazer Logout</a>
          </li>
        </ul>
      </div>
    </nav>
    <!-- Container Area Cliente -->
<section class="container">
<?php if ($msg) : ?>
    <div class="alert <?= $msg['classe'] ?>">
     <?= $msg['mensagem']; ?>
     <?php endif; ?>  
</section>
  <!--Nova Tabela -->
  <section class="container">
      <div class="text-center my-5">
        <h1 class="display-4 texto">Dados Cadastrais</h1>
      </div>
      <table class="table table-hover table-responsive-md">
        <thead>
          <tr class="texto">
            <th scope="col">CPF</th>
            <th scope="col">Nome</th>
            <th scope="col">Email</th>
            <th scope="col">Senha</th>
            <th scope="col">Endereço</th>
            <th scope="col">Cidade</th>
            <th scope="col">Estado</th>
            <th scope="col">Telefone</th>
            <th colspan="2"> Ações</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach ($lista_cliente as $cpf) : ?>
          <tr class="texto">
            <td><?= $cpf['cpf'] ?></td>
            <td><?= $cpf['nomeCompleto'] ?></td>
            <td><?= $cpf['email'] ?></td>
            <td><?= $cpf['senha'] ?></td>
            <td><?= $cpf['endereco'] ?></td>
            <td><?= $cpf['cidade'] ?></td>
            <td><?= $cpf['estado'] ?></td>
            <td><?= $cpf['telefone'] ?></td>
            <td>
              <a href="editarCliente.php?cpf=<?= $cpf['cpf'] ?>" class="btn btn-primary">Editar</a>
            </td>
            <td>
              <a class="btn btn-primary" href="#" data-bs-toggle="modal" data-bs-target="#loginModal">Excluir</a>
            </td>
          </tr>
         <?php endforeach; ?>
        </tbody>
      </table>
    </section>
  
      <!-- Modal -->
    <div class="modal" id="loginModal" tabindex="-1" aria-labelledby="modallogin" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <div class="modal-body">
            <form method="GET">
              <div class="form-group">
              <text>Deseja excluir o cadastro? </text>
              </div>
              <a href="areaCliente.php?excluir=<?= $cpf['cpf'] ?>" class="btn btn-danger">Excluir</a>
              <a href="areaCliente.php" class="btn btn-danger">Cancelar</a>
            </form>
          </div>
        </div>
      </div>
    </div>
  <!-- Fecha Modal  -->
  
    <div>
        <!-- Espaçamento para o Footer -->
        <section class="py-5">
          <br/><br/> <br/> <br/>
        </section>
    </div>
    
  <!-- footer section -->
  <footer class="footer_section">
    <div>
        <div class="container">
            <div class="row">
              <div class="col-md-3 col-6">
                <h4>PÁGINAS</h4>
                <ul class="list-unstyled">
                  <li><a href="contato.php" class="custom-nav">Contato</a></li>
                  <li><a href="inscricao.php" class="custom-nav">Resgistre-se</a></li>
                  <li><a href="login.php" class="custom-nav">Login</a></li>
                </ul>
              </div>
              <div class="col-md-3 col-6">
                <h4>FILIAIS</h4>
                <ul class="list-unstyled">
                  <li>Poços de Caldas MG</li>
                  <li>Av. Paulista SP</li>
                  <li>Barra da Tijuca RJ</li>
                </ul>
              </div>
              <div class="footer_contact col-md-4">
                <h4>
                  CONTATO
                </h4>
                <div class="contact_link_box">
                  <a href="#">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <span>
                      Central POÇOS DE CALDAS
                    </span>
                  </a>
                  <a href="#">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <span>
                      Tel +35 99199-7070
                    </span>
                  </a>
                  <a href="#">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <span>
                      grupo@alunos.ifsuldeminas.edu.br
                    </span>
                  </a>
                </div>
              </div>
              <div class="col-md-2">
                <h4>SOCIAL MEDIA</h4>
                <div class="footer_social">
                    <a href="https://pt-br.facebook.com/">
                      <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                    <a href="https://twitter.com/login?lang=pt">
                      <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                    <a href="https://br.linkedin.com/">
                      <i class="fa fa-linkedin" aria-hidden="true"></i>
                    </a>
                    <a href="https://www.instagram.com/">
                      <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                  </div>
              </div>
            </div>
          </div>
      <div class="footer-info">
        <p>
          &copy; <span id="displayYear"></span> Todos os direitos reservados By
          <a href="#">Gisele / Manoela / Welton</a><br><br>
        </p>
      </div>
    </div>
  </footer>
  <!-- FIM footer section -->
    
    <script type="text/javascript" src="js/jquery-3.2.1.slim.min.js"></script>
    <script type="text/javascript" src="js/popper.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
  </body>
</html>