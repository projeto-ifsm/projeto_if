<?php

session_start();
include_once("conecta.php");

$msg = array();

try {


  if ($_GET)
    {

$cpf = filter_var($_GET['cpf'], FILTER_SANITIZE_STRING) ?: throw new Exception('Preencha o CPF!');
$senha = filter_var($_GET['senha'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES) ?: throw new Exception('Por favor, preencha a senha!');


$cpf = mysqli_real_escape_string($conn, $cpf);
$senha = mysqli_real_escape_string($conn, $senha);

$query = "select * from cliente where cpf = '{$cpf}' and senha = '{$senha}'";

$result = mysqli_query($conn, $query);

$row = mysqli_num_rows($result);

if($row == 1) {
  $_SESSION['cpf'] = $cpf;
	header('Location: areaCliente.php');
	exit();
} else {
	$_SESSION['nao_autenticado'] = true;
	header('Location: inscricao.php');
	exit();
}


    }
}
catch(Exception $ex)
{
    $msg = array(
        'classe' => 'alert-danger',
        'mensagem' => $ex->getMessage()
    );
}


?>


<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="Restaurante IF PHP" />
  <meta name="description" content="Projeto Interdisciplinar" />
  <meta name="author" content="Gisele - Manu - Welton" />
  <link rel="shortcut icon" href="img/favicon.png" type="">
<!-- Bootstrap + Custom CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css" />
<!-- font awesome style -->
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <title> Restaurante IF | Login</title>
</head>
  <body> 

   <!-- Modal -->
   <div class="modal" id="loginModal" tabindex="-1" aria-labelledby="modallogin" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModal">Logar-se</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">     
          </button>
      </div>
      <div class="modal-body">
        <form method="GET" action="areaCliente.php">
          <div class="form-group">
            <label for="loginCPF">CPF</label>
            <input type="text" class="form-control" id="loginCPF"  name="cpf" value="<?= $_GET['cpf'] ?? '' ?>">
          </div>
          <div class="form-group">
            <label for="loginSenha">Senha</label>
            <input type="password" class="form-control" id="loginSenha" name="senha" value="<?= $_GET['senha'] ?? '' ?>">
          </div>
          <button type="submit" class="btn btn-success">Entrar na Conta</button>
          <small class="form-text text-muted">Esqueceu a senha? <a href="index.php">Clique aqui</a>.</small>
        </form>
      </div>
    </div>
  </div>
</div>
  <!-- Fecha Modal  -->

    <!-- NAVBAR MENU -->
    <nav class="navbar navbar-expand-md navbar-dark nav-bar-color fixed-top py-3 box-shadow">
      <a href="index.php" class="navbar-brand">
        <img src="img/Restauranteif.svg" class="mx-5" alt="Restauranteif">
      </a>
  
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
  
      <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
        <ul class="navbar-nav ml-auto  mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="contato.php">Contato</a>
          </li>
          <li class="nav-item">
          <a class="nav-link" href="#" data-bs-toggle="modal" data-bs-target="#loginModal">Login</a>
          </li>
          <li class="nav-item">
            <a class="btn btn-outline-success ml-md-5 mx-2" href="inscricao.php">Cadastre-se</a>
          </li>
        </ul>
      </div>
    </nav>
  <!-- Contato -->
    <section class="container">
      <div class="my-5 text-center">
        <span class="h6 d-block texto">POSSUI ALGUMA DÚVIDA?</span>
        <h1 class="display-4 text-success">Fale Conosco</h1>
      </div>
      <div class="row">
        <div class="col-lg mb-5">
          <form class="bg-light rounded p-4 box-shadow" action="">
            <div class="form-group">
              <label for="clienteNome">Nome</label>
              <input type="text" class="form-control" id="clienteNome">
            </div>
            <div class="form-group">
              <label for="clienteEmail">Email</label>
              <input type="email" class="form-control" id="clienteEmail">
            </div>
            <div class="form-group">
              <label for="clienteMensagem">Mensagem</label>
              <textarea id="clienteMensagem" class="form-control" rows="4"></textarea>
            </div>
            <button type="submit" class="btn btn-success">Enviar Mensagem</button>
          </form>
        </div>
        <div class="col-lg">
          <h2 class="h6 text-success">NOSSO ENDEREÇO</h2>
          <a href="#"><img class="img-fluid box-shadow rounded mb-4" src="img/endereco-pocos.png" alt="Endereço da Empresa"></a>
          
          <h2 class="h6 text-success">DADOS DE CONTATO</h2>
          <ul class="list-unstyled texto">
            <li>grupo@alunos.ifsuldeminas.edu.br</li>
            <li>35 99999-9999</li>
            <li>De Seg. à Sex. das 8h às 18h</li>
          </ul>
        </div>
      </div>
    </section>
    
    <section class="container">
      <div class="my-5 text-center">
        <span class="h6 d-block texto">AINDA ESTÁ COM DÚVIDAS?</span>
        <h2 class="display-4 text-success">Perguntas Frequentes</h2>
      </div>
      <div class="row justify-content-center">
        <div class="col-md-6" id="perguntasFrequentes" data-children=".pergunta">
          <div class="pergunta py-2">
            <a class="lead texto custom-nav" data-toggle="collapse" data-parent="#perguntasFrequentes" href="#pergunta1" aria-expanded="true" aria-controls="pergunta1">→ É possível cancelar uma reserva?</a>
            <div id="pergunta1" class="collapse show" role="tabpanel">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pretium lorem non vestibulum scelerisque. Proin a vestibulum sem, eget tristique massa. Aliquam lacinia rhoncus nibh quis ornare.</p>
            </div>
          </div>
          <div class="dropdown-divider"></div>
          <div class="pergunta py-2">
            <a class="lead texto custom-nav" data-toggle="collapse" data-parent="#perguntasFrequentes" href="#pergunta2" aria-expanded="true" aria-controls="pergunta2">→ Politica de reembolso?</a>
            <div id="pergunta2" class="collapse" role="tabpanel">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pretium lorem non vestibulum scelerisque. Proin a vestibulum sem, eget tristique massa. Aliquam lacinia rhoncus nibh quis ornare.</p>
            </div>
          </div>
          <div class="dropdown-divider"></div>
          <div class="pergunta py-2">
            <a class="lead texto custom-nav" data-toggle="collapse" data-parent="#perguntasFrequentes" href="#pergunta3" aria-expanded="true" aria-controls="pergunta3">→ O Delivery funciona 24 Horas?</a>
            <div id="pergunta3" class="collapse" role="tabpanel">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pretium lorem non vestibulum scelerisque. Proin a vestibulum sem, eget tristique massa. Aliquam lacinia rhoncus nibh quis ornare.</p>
            </div>
          </div>
          <div class="dropdown-divider"></div>
          <div class="pergunta py-2">
            <a class="lead texto custom-nav" data-toggle="collapse" data-parent="#perguntasFrequentes" href="#pergunta4" aria-expanded="true" aria-controls="pergunta4">→ Trabalham com Buffet eventos?</a>
            <div id="pergunta4" class="collapse" role="tabpanel">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pretium lorem non vestibulum scelerisque. Proin a vestibulum sem, eget tristique massa. Aliquam lacinia rhoncus nibh quis ornare.</p>
            </div>
          </div>
          <div class="dropdown-divider"></div>
          <div class="pergunta py-2">
            <a class="lead texto custom-nav" data-toggle="collapse" data-parent="#perguntasFrequentes" href="#pergunta5" aria-expanded="true" aria-controls="pergunta5">→ Perdi minha senha e esqueci meu e-mail</a>
            <div id="pergunta5" class="collapse" role="tabpanel">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pretium lorem non vestibulum scelerisque. Proin a vestibulum sem, eget tristique massa. Aliquam lacinia rhoncus nibh quis ornare.</p>
            </div>
          </div>
          <div class="dropdown-divider"></div>
        </div>
      </div>
      <div class="text-center my-4">
        <p class="small m-1 texto">FALE CONOSCO PARA MAIS DÚVIDAS</p>
        <a class="btn btn-success btn-lg" href="index.php">Entre em Contato</a>
      </div>
    </section>
    
  <!-- footer section -->
  <footer class="footer_section">
    <div>
        <div class="container">
            <div class="row">
              <div class="col-md-3 col-6">
                <h4>PÁGINAS</h4>
                <ul class="list-unstyled">
                  <li><a href="contato.php" class="custom-nav">Contato</a></li>
                  <li><a href="inscricao.php" class="custom-nav">Resgistre-se</a></li>
                  <li><a href="login.php" class="custom-nav" data-bs-toggle="modal" data-bs-target="#loginModal">Login</a></li>
                </ul>
              </div>
              <div class="col-md-3 col-6">
                <h4>FILIAIS</h4>
                <ul class="list-unstyled">
                  <li>Poços de Caldas MG</li>
                  <li>Av. Paulista SP</li>
                  <li>Barra da Tijuca RJ</li>
                </ul>
              </div>
              <div class="footer_contact col-md-4">
                <h4>
                  CONTATO
                </h4>
                <div class="contact_link_box">
                  <a href="#">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <span>
                      Central POÇOS DE CALDAS
                    </span>
                  </a>
                  <a href="#">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <span>
                      Tel +35 99199-7070
                    </span>
                  </a>
                  <a href="#">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <span>
                      grupo@alunos.ifsuldeminas.edu.br
                    </span>
                  </a>
                </div>
              </div>
              <div class="col-md-2">
                <h4>SOCIAL MEDIA</h4>
                <div class="footer_social">
                    <a href="https://pt-br.facebook.com/">
                      <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                    <a href="https://twitter.com/login?lang=pt">
                      <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                    <a href="https://br.linkedin.com/">
                      <i class="fa fa-linkedin" aria-hidden="true"></i>
                    </a>
                    <a href="https://www.instagram.com/">
                      <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                  </div>
              </div>
            </div>
          </div>
      <div class="footer-info">
        <p>
          &copy; <span id="displayYear"></span> Todos os direitos reservados By
          <a href="#">Gisele / Manoela / Welton</a><br><br>
        </p>
      </div>
    </div>
  </footer>
  <!-- FIM footer section -->
    
    <script type="text/javascript" src="js/jquery-3.2.1.slim.min.js"></script>
    <script type="text/javascript" src="js/popper.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
  </body>
</html>