<?php

session_start();
include_once("conecta.php");

$msg = array();

try 
{
    if ($_POST)
    {
        $nomeCompleto = filter_var($_POST['nomeCompleto'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES) ?: throw new Exception('Por favor, preencha o campo Nome!');
        $cpf = filter_var($_POST['cpf'], FILTER_SANITIZE_STRING) ?: throw new Exception('Preencha o CPF!');
        $email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ?: throw new Exception('E-mail inválido!');
        $senha = filter_var($_POST['senha'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES) ?: throw new Exception('Por favor, preencha a senha!');
        $endereco = filter_var($_POST['endereco'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES) ?: throw new Exception('Por favor, preencha o endereço!');
        $cidade = filter_var($_POST['cidade'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES) ?: throw new Exception('Por favor, preencha a cidade!');
        $estado = filter_var($_POST['estado'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES) ?: throw new Exception('Por favor, selecione o estado!');
        $telefone = filter_var($_POST['telefone'], FILTER_SANITIZE_STRING) ?: throw new Exception('Preencha o telefone!');

        $nomeCompleto = mysqli_real_escape_string($conn, $nomeCompleto);
        $cpf = mysqli_real_escape_string($conn, $cpf);
        $email = mysqli_real_escape_string($conn, $email);
        $senha = mysqli_real_escape_string($conn, $senha);
        $endereco = mysqli_real_escape_string($conn, $endereco);
        $cidade = mysqli_real_escape_string($conn, $cidade);
        $estado = mysqli_real_escape_string($conn, $estado);
        $telefone = mysqli_real_escape_string($conn, $telefone);
        $sql = "INSERT INTO cliente (nomeCompleto, cpf, email, senha, endereco, cidade, estado, telefone) VALUES('$nomeCompleto', '$cpf', '$email', '$senha', '$endereco', '$cidade', '$estado', '$telefone')";

        
        $resultado = mysqli_query($conn, $sql);

        
        if ($resultado === false || mysqli_errno($conn)) {
            throw new Exception('Erro ao realizar operação no banco de dados: ' . mysqli_error($conn));
        }

        $msg = array(

            'classe' => 'alert-success',
            'mensagem' => 'Cliente cadastrado com sucesso!'
        );
    }
}
catch(Exception $ex)
{
    $msg = array(
        'classe' => 'alert-danger',
        'mensagem' => $ex->getMessage()
    );
}


try {


  if ($_GET)
    {

$cpf = filter_var($_GET['cpf'], FILTER_SANITIZE_STRING) ?: throw new Exception('Preencha o CPF!');
$senha = filter_var($_GET['senha'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES) ?: throw new Exception('Por favor, preencha a senha!');


$cpf = mysqli_real_escape_string($conn, $cpf);
$senha = mysqli_real_escape_string($conn, $senha);

$query = "select * from cliente where cpf = '{$cpf}' and senha = '{$senha}'";

$result = mysqli_query($conn, $query);

$row = mysqli_num_rows($result);

if($row == 1) {
  $_SESSION['cpf'] = $cpf;
	header('Location: areaCliente.php');
	exit();
} else {
	$_SESSION['nao_autenticado'] = true;
	header('Location: inscricao.php');
	exit();
}


    }
}
catch(Exception $ex)
{
    $msg = array(
        'classe' => 'alert-danger',
        'mensagem' => $ex->getMessage()
    );
}


?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="Restaurante IF PHP" />
  <meta name="description" content="Projeto Interdisciplinar" />
  <meta name="author" content="Gisele - Manu - Welton" />
  <link rel="shortcut icon" href="img/favicon.png" type="">
<!-- Bootstrap + Custom CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css" />
<!-- font awesome style -->
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <title> Restaurante IF | Cadastro</title>
</head>

 <!-- Modal -->
 <div class="modal" id="loginModal" tabindex="-1" aria-labelledby="modallogin" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModal">Logar-se</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">     
          </button>
      </div>
      <div class="modal-body">
        <form method="GET">
          <div class="form-group">
            <label for="loginCPF">CPF</label>
            <input type="text" class="form-control" id="loginCPF"  name="cpf" value="<?= $_GET['cpf'] ?? '' ?>">
          </div>
          <div class="form-group">
            <label for="loginSenha">Senha</label>
            <input type="password" class="form-control" id="loginSenha" name="senha" value="<?= $_GET['senha'] ?? '' ?>">
          </div>
          <button type="submit" class="btn btn-success">Entrar na Conta</button>
          <small class="form-text text-muted">Esqueceu a senha? <a href="index.php">Clique aqui</a>.</small>
        </form>
      </div>
    </div>
  </div>
</div>

  <!-- Fecha Modal  -->


  <body> 
    <!-- NAVBAR MENU -->
    <nav class="navbar navbar-expand-md navbar-dark nav-bar-color fixed-top py-3 box-shadow">
      <a href="index.php" class="navbar-brand">
        <img src="img/Restauranteif.svg" class="mx-5" alt="Restauranteif">
      </a>
  
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
  
      <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
        <ul class="navbar-nav ml-auto  mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="contato.php">Contato</a>
          </li>
          <li class="nav-item">
          <a class="nav-link" href="#" data-bs-toggle="modal" data-bs-target="#loginModal">Login</a>
          </li>
          <li class="nav-item">
            <a class="btn btn-outline-success ml-md-5 mx-2" href="inscricao.php">Cadastre-se</a>
          </li>
        </ul>
      </div>
    </nav>
    <!-- Forms -->

<!-- Se tiver alguma msg de erro para ser exibida -->
<?php if ($msg) : ?>
            <div class="alert <?= $msg['classe'] ?>">
                <?= $msg['mensagem']; ?>
            </div>
        <?php endif; ?>


    
   <section class="container font-forms" >
      <div class="my-5 text-center">
        <h1 class="display-4 texto">Cadastro</h1>
      </div>
      <div class="row justify-content-center texto">
        <form class="col-lg-6" method="POST">
          <div class="form-row py-4">
            <div class="form-group col-md-12">
              <label for="inputNome">Nome</label>
              <input type="text" class="form-control" id="inputNome" name= nomeCompleto placeholder="Nome completo" value="<?= $_POST['nomeCompleto'] ?? '' ?>">
            </div>
            <div class="form-group col-md-12">
              <label for="inputCPF">CPF</label>
              <input type="text" class="form-control" id="inputCPF" name="cpf" placeholder="xxx.xxx.xxx-xx" value="<?= $_POST['cpf'] ?? '' ?>">
            </div>
            <div class="form-group col-md-12">
              <label for="inputEmail">Email</label>
              <input type="email" class="form-control" id="inputEmail" name= "email" placeholder="Seu email" value="<?= $_POST['email'] ?? '' ?>">
            </div>
            <div class="form-group col-md-12">
              <label for="inputSenha">Senha</label>
              <input type="password" class="form-control" id="inputSenha" name="senha" placeholder="Senha de acesso" value="<?= $_POST['senha'] ?? '' ?>">
            </div>
            <div class="form-group col-12">
              <label for="inputEndereco">Endereço</label>
              <input type="text" class="form-control" id="inputEndereco" name="endereco" placeholder="Número, nome da rua e bairro" value="<?= $_POST['endereco'] ?? '' ?>">
            </div>
            <div class="form-group col-md-12">
              <label for="inputCidade">Cidade</label>
              <input type="text" class="form-control" id="inputCidade" name="cidade" placeholder="Cidade" value="<?= $_POST['cidade'] ?? '' ?>">
            </div>
            <div class="form-group col-md-6 col-12">
              <label for="inputCidade">Estado</label>
              <select  name="estado" id="inputCidade" name="estado" class="form-control" value="<?= $_POST['estado'] ?? '' ?>">
                <option disabled selected>Selecione o estado </option>
                <option>Acre</option>
                <option>Alagoas</option>
                <option>Amapá</option>
                <option>Amazonas</option>
                <option>Bahia</option>
                <option>Ceará</option>
                <option>Distrito Federal</option>
                <option>Espírito Santo</option>
                <option>Goiás</option>
                <option>Maranhão</option>
                <option>Mato Grosso</option>
                <option>Mato Grosso do Sul</option>
                <option>Minas Gerais</option>
                <option>Pará</option>
                <option>Paraíba</option>
                <option>Paraná</option>
                <option>Pernambuco</option>
                <option>Piauí</option>
                <option>Rio de Janeiro</option>
                <option>Rio Grande do Norte</option>
                <option>Rio Grande do Sul</option>
                <option>Rondônia</option>
                <option>Roraima</option>
                <option>Santa Catarina</option>
                <option>São Paulo</option>
                <option>Sergipe</option>
                <option>Tocantins</option>
              </select>
            </div>
            <div class="form-group col-md-6 col-12">
              <label for="inputTelefone">Telefone</label>
              <input type="text" class="form-control" id="inputTelefone" name="telefone" placeholder="xx-xxxxx-xxx" value="<?= $_POST['telefone'] ?? '' ?>">
            </div>
          </div>

          <div class="form-check">
            <label class="form-check-label">
              <input type="checkbox" class="form-check-input" checked required>
              Concordo com os <a href="#">Termos e Condições</a>.
            </label>
          </div>
          <div class="py-2">
            <button button type="submit" class="btn btn-success btn-lg">Finalizar Cadastro</button>
          </div>
          
          </form>
      </div>
     </section>

    
  <!-- footer section -->
  <footer class="footer_section">
    <div>
        <div class="container">
            <div class="row">
              <div class="col-md-3 col-6">
                <h4>PÁGINAS</h4>
                <ul class="list-unstyled">
                  <li><a href="contato.php" class="custom-nav">Contato</a></li>
                  <li><a href="inscricao.php" class="custom-nav">Resgistre-se</a></li>
                  <li><a href="login.php" class="custom-nav" data-bs-toggle="modal" data-bs-target="#loginModal">Login</a></li>
                </ul>
              </div>
              <div class="col-md-3 col-6">
                <h4>FILIAIS</h4>
                <ul class="list-unstyled">
                  <li>Poços de Caldas MG</li>
                  <li>Av. Paulista SP</li>
                  <li>Barra da Tijuca RJ</li>
                </ul>
              </div>
              <div class="footer_contact col-md-4">
                <h4>
                  CONTATO
                </h4>
                <div class="contact_link_box">
                  <a href="#">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <span>
                      Central POÇOS DE CALDAS
                    </span>
                  </a>
                  <a href="#">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <span>
                      Tel +35 99199-7070
                    </span>
                  </a>
                  <a href="#">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <span>
                      grupo@alunos.ifsuldeminas.edu.br
                    </span>
                  </a>
                </div>
              </div>
              <div class="col-md-2">
                <h4>SOCIAL MEDIA</h4>
                <div class="footer_social">
                    <a href="https://pt-br.facebook.com/">
                      <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                    <a href="https://twitter.com/login?lang=pt">
                      <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                    <a href="https://br.linkedin.com/">
                      <i class="fa fa-linkedin" aria-hidden="true"></i>
                    </a>
                    <a href="https://www.instagram.com/">
                      <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                  </div>
              </div>
            </div>
          </div>
      <div class="footer-info">
        <p>
          &copy; <span id="displayYear"></span> Todos os direitos reservados By
          <a href="#">Gisele / Manoela / Welton</a><br><br>
        </p>
      </div>
    </div>
  </footer>
  <!-- FIM footer section -->
    
    <script type="text/javascript" src="js/jquery-3.2.1.slim.min.js"></script>
    <script type="text/javascript" src="js/popper.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
  </body>
</html>