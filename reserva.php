<?php

session_start();
include_once("conecta.php");

$msg = array();

try 
{
    if ($_POST)
    {
      $nomeCompleto = filter_input(INPUT_POST, 'nomeCompleto', FILTER_SANITIZE_STRING) ?: throw new Exception('Por favor, informe o Nome!');
      $celular = filter_input(INPUT_POST, 'celular', FILTER_SANITIZE_STRING) ?: throw new Exception('Por favor, informar um número de celular!');
      $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL) ?: throw new Exception('Por favor, informar um e-mail válido!');
      $numeroPessoas = filter_input(INPUT_POST, 'numeroPessoas', FILTER_SANITIZE_STRING) ?: throw new Exception('Por favor, selecione o número de pessoas!');
      $data = filter_input(INPUT_POST, 'data', FILTER_SANITIZE_STRING) ?: throw new Exception('Por favor, informe uma data!');
      $filial = filter_input(INPUT_POST, 'filial', FILTER_SANITIZE_STRING) ?: throw new Exception('Por favor, selecione a filiar desejada!');
      
            
      
      $fazer_reserva = "INSERT INTO reserva (nomeCompleto, celular, email, numeroPessoas, data, filial) VALUES('$nomeCompleto', '$celular', '$email', '$numeroPessoas', '$data','$filial')";
      $reserva_feita = mysqli_query($conn,$fazer_reserva );
           
        if ($reserva_feita === false || mysqli_errno($conn)) {
            throw new Exception('Erro ao realizar operação no banco de dados: ' . mysqli_error($conn));
        }

        $msg = array(

            'classe' => 'alert-success',
            'mensagem' => 'Reserva feita com sucesso!'
        );
    }
}
catch(Exception $ex)
{
    $msg = array(
        'classe' => 'alert-danger',
        'mensagem' => $ex->getMessage()
    );
}




try {


  if ($_GET)
    {

$cpf = filter_var($_GET['cpf'], FILTER_SANITIZE_STRING) ?: throw new Exception('Preencha o CPF!');
$senha = filter_var($_GET['senha'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES) ?: throw new Exception('Por favor, preencha a senha!');


$cpf = mysqli_real_escape_string($conn, $cpf);
$senha = mysqli_real_escape_string($conn, $senha);

$query = "select * from cliente where cpf = '{$cpf}' and senha = '{$senha}'";

$result = mysqli_query($conn, $query);

$row = mysqli_num_rows($result);

if($row == 1) {
  $_SESSION['cpf'] = $cpf;
	header('Location: areaCliente.php');
	exit();
} else {
	$_SESSION['nao_autenticado'] = true;
	header('Location: inscricao.php');
	exit();
}


    }
}
catch(Exception $ex)
{
    $msg = array(
        'classe' => 'alert-danger',
        'mensagem' => $ex->getMessage()
    );
}


?>


<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="Restaurante IF PHP" />
  <meta name="description" content="Projeto Interdisciplinar" />
  <meta name="author" content="Gisele - Manu - Welton" />
  <link rel="shortcut icon" href="img/favicon.png" type="">
<!-- Bootstrap + Custom CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css" />
<!-- font awesome style -->
<link href="css/font-awesome.min.css" rel="stylesheet" />
  <title> Restaurante IF </title>
</head>
<body>

<!-- Modal -->
<div class="modal" id="loginModal" tabindex="-1" aria-labelledby="modallogin" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModal">Logar-se</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">     
          </button>
      </div>
      <div class="modal-body">
        <form method="GET" action="areaCliente.php">
          <div class="form-group">
            <label for="loginCPF">CPF</label>
            <input type="text" class="form-control" id="loginCPF"  name="cpf" value="<?= $_GET['cpf'] ?? '' ?>">
          </div>
          <div class="form-group">
            <label for="loginSenha">Senha</label>
            <input type="password" class="form-control" id="loginSenha" name="senha" value="<?= $_GET['senha'] ?? '' ?>">
          </div>
          <button type="submit" class="btn btn-success">Entrar na Conta</button>
          <small class="form-text text-muted">Esqueceu a senha? <a href="index.php">Clique aqui</a>.</small>
        </form>
      </div>
    </div>
  </div>
</div>
  <!-- Fecha Modal  -->


    <!-- NAVBAR MENU -->
    <nav class="navbar navbar-expand-md navbar-dark nav-bar-color fixed-top py-3 box-shadow">
      <a href="index.php" class="navbar-brand">
        <img src="img/Restauranteif.svg" class="mx-5" alt="Restauranteif">
      </a>
  
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
  
      <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
        <ul class="navbar-nav ml-auto  mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="contato.php">Contato</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#" data-bs-toggle="modal" data-bs-target="#loginModal">Login</a>
          </li>
          <li class="nav-item">
            <a class="btn btn-outline-success ml-md-5 mx-2" href="inscricao.php">Cadastre-se</a>
          </li>
        </ul>
      </div>
    </nav>
<!-- Reservas section -->

<?php if ($msg) : ?>
            <div class="alert <?= $msg['classe'] ?>">
                <?= $msg['mensagem']; ?>
            </div>
        <?php endif; ?>

        
<section class="book_section layout_padding">
    <div class="container">
      <div class="heading_container">
        <h2 class="py-4 display-6 texto">
            Faça sua Reserva
        </h2>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form_container">
            <form  method="POST">
              <div>
                <input type="text" name="nomeCompleto" class="form-control" placeholder="Seu nome" value="<?= $_POST['nomeCompleto'] ?? '' ?>" />
              </div>
              <div>
                <input type="text" name="celular" class="form-control" placeholder="Celular" value="<?= $_POST['celular'] ?? '' ?>"/>
              </div>
              <div>
                <input type="email" name="email" class="form-control" placeholder="E-mail" value="<?= $_POST['email'] ?? '' ?>" />
              </div>
              <div>
                <select class="form-control nice-select wide" name="numeroPessoas" value="<?= $_POST['numeroPessoas'] ?? '' ?>">
                  <option value="" disabled selected>
                    Quantas pessoas?
                  </option>
                  <option>
                    2
                  </option>
                  <option>
                    3
                  </option>
                  <option>
                    4
                  </option>
                  <option>
                    5
                  </option>
                </select>
              </div>
              <div>
                <input type="date" name="data"class="form-control" value="<?= $_POST['data'] ?? '' ?>">
              </div>
              <div>
              <select class="form-select" name="filial" aria-label="Default select example" value="<?= $_POST['filial'] ?? '' ?>">
                <option disabled selected>Selecione uma filial</option>
                <option value="1">Poços de Caldas</option>
                <option value="2">São Paulo</option>
                <option value="3">Rio de Janeiro</option>
              </select>
              </div>
              <div class="py-3">
              <button button type="submit" class="btn btn-success btn-lg">Reservar</button>
              </div>
            </form>
          </div>
        </div>
        <div class="col-md-6">
          <div class="map_container ">
            <div id="googleMap"></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end book section -->
  <!-- Voltar página anterior -->
    <nav>
        <ul class="pagination justify-content-center">
        <li class="page-item enable">
        <a class="texto custom-nav" href="areaCliente.php" tabindex="-1"><b>&larr;Voltar a página cliente</b></a>
        </li>
        </ul>
  </nav>
  <!-- Espaçamento do Footer-->
  <section>
      <div class="py-5"></div>
  </section>
  <!-- footer section -->
  <footer class="footer_section">
    <div>
        <div class="container">
            <div class="row">
              <div class="col-md-3 col-6">
                <h4>PÁGINAS</h4>
                <ul class="list-unstyled">
                  <li><a href="contato.php" class="custom-nav">Contato</a></li>
                  <li><a href="inscricao.php" class="custom-nav">Resgistre-se</a></li>
                  <li><a href="login.php" class="custom-nav" data-bs-toggle="modal" data-bs-target="#loginModal">Login</a></li>
                </ul>
              </div>
              <div class="col-md-3 col-6">
                <h4>FILIAIS</h4>
                <ul class="list-unstyled">
                  <li>Poços de Caldas MG</li>
                  <li>Av. Paulista SP</li>
                  <li>Barra da Tijuca RJ</li>
                </ul>
              </div>
              <div class="footer_contact col-md-4">
                <h4>
                  CONTATO
                </h4>
                <div class="contact_link_box">
                  <a href="#">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <span>
                      Central POÇOS DE CALDAS
                    </span>
                  </a>
                  <a href="#">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <span>
                      Tel +35 99199-7070
                    </span>
                  </a>
                  <a href="#">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <span>
                      grupo@alunos.ifsuldeminas.edu.br
                    </span>
                  </a>
                </div>
              </div>
              <div class="col-md-2">
                <h4>SOCIAL MEDIA</h4>
                <div class="footer_social">
                    <a href="https://pt-br.facebook.com/">
                      <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                    <a href="https://twitter.com/login?lang=pt">
                      <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                    <a href="https://br.linkedin.com/">
                      <i class="fa fa-linkedin" aria-hidden="true"></i>
                    </a>
                    <a href="https://www.instagram.com/">
                      <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                  </div>
              </div>
            </div>
          </div>
      <div class="footer-info">
        <p>
          &copy; <span id="displayYear"></span> Todos os direitos reservados By
          <a href="#">Gisele / Manoela / Welton</a><br><br>
        </p>
      </div>
    </div>
  </footer>

 <!-- jQery -->
 <script src="js/jquery-3.4.1.min.js"></script>
  <!-- popper js -->
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <!-- bootstrap js -->
  <script src="js/bootstrap.js"></script>
  <!-- owl slider -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js">
  </script>
  <!-- isotope js -->
  <script src="https://unpkg.com/isotope-layout@3.0.4/dist/isotope.pkgd.min.js"></script>
  <!-- nice select -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js"></script>
  <!-- custom js -->
  <script src="js/custom.js"></script>
  <!-- Google Map -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap">
  </script>
  <!-- FIM footer section -->